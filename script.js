$(document).ready(function(){
	$('.menubtn').click(function() {
		setActiveTab($(this).index());
	});

	setActiveTab(0);
});

function setActiveTab(index){
	$('.menubtn').removeClass('current');
	$('#content').children().hide();

	$($('#menu').children()[index]).addClass('current');
	$($('#content').children()[index]).show();
};